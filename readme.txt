  _____                                 _           
 |  __ \                               (_)          
 | |__) |___  ___ _ __   ___  _ __  ___ ___   _____ 
 |  _  // _ \/ __| '_ \ / _ \| '_ \/ __| \ \ / / _ \
 | | \ \  __/\__ \ |_) | (_) | | | \__ \ |\ V /  __/
 |_|__\_\___||___/ .__/ \___/|_| |_|___/_| \_/_\___|
 (_)  ____|      | |                    | |/ ____|  
  _| |__ _ __ __ |_| __ ___   ___       | | (___    
 | |  __| '__/ _` | '_ ` _ \ / _ \  _   | |\___ \   
 | | |  | | | (_| | | | | | |  __/ | |__| |____) |  
 |_|_|  |_|  \__,_|_| |_| |_|\___|  \____/|_____/   
                                                    
                                                    
ResponsiveIframesJs
 
Resizes iframes to be responsive

 = Providers =
 Providers are the iframe embed services that are supported
 To add new provider just add a new with addProvider-method
 ResponsiveIframesJs.addProvider({
      'name': 'Facebook',
      'regex': /facebook.com.*post.php/,
      'responsive': false,
      'function': function(iframe) { 
              // Do cutom stuff here... 
          }
  }, true);
  
  If responsive is true, then we try to do a basic method to make
  iframe responsive. It requies that the content is reponsive.
  
 = Events =
 resizeAll, Event that triggers the resize of all iframes
 
 @requires jQuery
 @author Jamie Telin, jamie.telin@gmail.com
 @url https://bitbucket.org/jamietelin/responsiveiframesjs
 @version 1.0