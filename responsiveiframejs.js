/**
 * ResponsiveIframesJs
 * 
 * Resizes iframes to be responsive
 *
 * = Providers =
 * Providers are the iframe embed services that are supported
 * To add new provider just add a new with addProvider-method
 * ResponsiveIframesJs.addProvider({
 *      'name': 'Facebook',
 *      'regex': /facebook.com.*post.php/,
 *      'responsive': false,
 *      'function': function(iframe) { 
 *              // Do cutom stuff here... 
 *          }
 *  }, true);
 *  
 *  If responsive is true, then we try to do a basic method to make
 *  iframe responsive. It requies that the comntent is reponsive.
 *  
 * = Events =
 * resizeAll, Event that triggers the resize of all iframes
 * 
 * @requires jQuery
 * @author Jamie Telin, jamie.telin@gmail.com
 * @version 1.0
 **/
var ResponsiveIframesJs = (function($) {

    function ResponsiveIframesJs() {
        this.isRunning = false;
        this.isDomReady = false;
        this.config = {
            'windowResize': true
        };

        this.events = new Array(
                'resizeAll'
                );

        this.providers = new Array();

        this.addDefaultProvider();

        $.each(this.events, function(index, value) {
            $(document).on(value, function() {
                this.resizeAll();
            }.bind(this));
        }.bind(this));

        $(function() {
            this.domReady();
            this.windowResize();
        }.bind(this));
    }

    ResponsiveIframesJs.prototype.addDefaultProvider = function() {

        this.addProvider({
            'name': 'Qbrick',
            'regex': /qbrick.com/,
            'responsive': false,
            'function': null
        }, false);

        this.addProvider({
            'name': 'Solidtango',
            'regex': /solidtango.com/,
            'responsive': false,
            'function': null
        }, false);

        this.addProvider({
            'name': 'Vimeo',
            'regex': /player.vimeo.com\/video/,
            'responsive': true,
            'function': null
        }, false);

        this.addProvider({
            'name': 'YouTube',
            'regex': /youtube.com/,
            'responsive': true,
            'function': null
        }, false);
    };

    ResponsiveIframesJs.prototype.addProvider = function(provider, triggerEvent) {
        triggerEvent = (typeof triggerEvent === 'undefined') ? true : triggerEvent;

        if (provider.regex instanceof RegExp) {

            provider.name = (typeof provider.name === 'undefined') ? 'Anonymous Provider' : provider.name;
            provider.responsive = (typeof provider.responsive === 'undefined') ? true : provider.responsive;
            provider.function = (typeof provider.function === 'undefined') ? null : provider.function;

            this.providers.push(provider);

            if (triggerEvent && !this.isRunning) {
                $(document).trigger("resizeAll");
            }
        } else {
            console.log('ResponsiveIframesJs.addProvider -> RegExp is not valid');
        }
    };

    ResponsiveIframesJs.prototype.domReady = function() {
        this.isDomReady = true;
        $(document).trigger("resizeAll");
    };

    ResponsiveIframesJs.prototype.windowResize = function() {
        if (this.config.windowResize === true) {

            $(window).resize(function() {

                clearTimeout($.data(this, 'resizeTimer'));

                $.data(this, 'resizeTimer', setTimeout(function() {

                    $(document).trigger("resizeAll");

                }, 200));

            });
        }
    };

    ResponsiveIframesJs.prototype.getSrcProvider = function(src) {
        var match = null;
        $.each(this.providers, function(index, obj) {
            if (typeof src == 'string' && src.match(obj.regex) !== null) {
                match = obj;
                return;
            }
            return;
        });
        return match;
    };

    ResponsiveIframesJs.prototype.resizeAll = function() {
        if (!this.isDomReady) {
            return 0;
        }
        this.isRunning = true;
        var self = this;
        var iframeCount = 0;
        // Find each iframe on page
        $("iframe").each(function(index) {
            var src = $(this).attr('src');

            var provider = self.getSrcProvider(src);

            if (provider !== null) {
                self.resize(this, provider);
                iframeCount++;
            }
        });

        return iframeCount;
    };

    ResponsiveIframesJs.prototype.resize = function(iframe, provider) {

        if (provider.responsive !== false) {
            this.standardResponsiveEmbed(iframe);
        }

        if (this.isFunction(provider.function)) {
            var func = provider.function.bind(this);
            func(iframe);
        } else {
            if (provider.responsive === false) {
                this.standardUnresponsiveEmbed(iframe);
            }
        }

        return;
    };

    ResponsiveIframesJs.prototype.isFunction = function(obj) {
        var getType = {};
        return obj && getType.toString.call(obj) === '[object Function]';
    };

    ResponsiveIframesJs.prototype.standardResponsiveEmbed = function(iframe) {
        var attrWidth = parseInt($(iframe).attr('width'));
        var attrHeight = parseInt($(iframe).attr('height'));
        var newWidth = Math.floor($(iframe).parent().width());
        var newHeight = Math.floor(attrHeight / attrWidth * newWidth);

        // Set new width and height based on parents width
        $(iframe).width(newWidth);
        $(iframe).height(newHeight);
        $(iframe).attr('width', newWidth);
        $(iframe).attr('height', newHeight);
    };

    ResponsiveIframesJs.prototype.standardUnresponsiveEmbed = function(iframe) {
        var attrWidth = parseInt($(iframe).attr('width'));
        var attrHeight = parseInt($(iframe).attr('height'));
        var newWidth = Math.floor($(iframe).parent().width());
        var newHeight = Math.floor(attrHeight / attrWidth * newWidth);
        var src = $(iframe).attr('src');

        console.log(iframe, src);

        // Set new width and height based on parents width
        $(iframe).width(newWidth);
        $(iframe).height(newHeight);
        $(iframe).attr('width', newWidth);
        $(iframe).attr('height', newHeight);

        // Update the src width and height
        src = src.replace('width=' + attrWidth, 'width=' + newWidth);
        src = src.replace('height=' + attrHeight, 'height=' + newHeight);
        $(iframe).attr('src', src);
    };

    ResponsiveIframesJs.prototype.facebookPostEmbed = function(iframe) {
        var attrWidth = Math.floor($(iframe).parent().width());
        var attrHeight = Math.floor($(iframe).parent().height());
        var newWidth = Math.floor($(iframe).parent().parent().parent().width());
        var newHeight = Math.floor(attrHeight / attrWidth * newWidth);
        var src = $(iframe).attr('src');

        // Set new width and height based on parents width
        $(iframe).width(newWidth);
        $(iframe).height(newHeight);
        $(iframe).attr('width', newWidth);
        $(iframe).attr('height', newHeight);
        $(iframe).parent().css('width', newWidth);
        $(iframe).parent().css('height', newHeight);

        // Update the src width and height
        src = src + '&width=' + newWidth;
        $(iframe).attr('src', src);
    };

    return new ResponsiveIframesJs();

})(jQuery);