/**
 * ResponsiveIframesJs Addon
 * 
 * Add Facebook Post Embed support
 * to ResponsiveIframesJs
 */
(function($) {
    
    // Add Facebook as provider
    var FacebookFunc = function(iframe) {
        var attrWidth = Math.floor($(iframe).parent().width());
        var attrHeight = Math.floor($(iframe).parent().height());
        var newWidth = Math.floor($(iframe).parent().parent().parent().width());
        var newHeight = Math.floor(attrHeight / attrWidth * newWidth);
        var src = $(iframe).attr('src');

        // Set new width and height based on parents width
        $(iframe).width(newWidth);
        $(iframe).height(newHeight);
        $(iframe).attr('width', newWidth);
        $(iframe).attr('height', newHeight);
        $(iframe).parent().css('width', newWidth);
        $(iframe).parent().css('height', newHeight);

        // Update the src width and height
        src = src + '&width=' + newWidth;
        $(iframe).attr('src', src);
    };
    
    ResponsiveIframesJs.addProvider({
        'name': 'Facebook',
        'regex': /facebook.com.*post.php/,
        'responsive': false,
        'function': FacebookFunc
    });
    
    $(function() {
        var fbPost = $('.fb-post');
        if ($(fbPost).length === 0) {
            return;
        }

        MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

        var observer = new MutationObserver(function(mutations, observer) {
            $(document).trigger("resizeAll");
            observer.disconnect();
        });

        // define what element should be observed by the observer
        // and what types of mutations trigger the callback
        observer.observe($(fbPost)[0], {
            subtree: false,
            attributes: false,
            childList: true
        });
    });

})(jQuery);